
#include "g_local.h"

vec_t getRangeToEnemy ( edict_t* _object )
{
	vec3_t dir;
	VectorSubtract ( _object->enemy->s.origin, _object->s.origin, dir );
	return VectorLength( dir );
}
