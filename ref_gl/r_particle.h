#ifndef _R_PARTICLE_H_
#define _R_PARTICLE_H_

#define PT_DEFAULT 0
#define PT_BUBBLE  1
#define PT_FLY     2
#define PT_BLOOD   3
#define PT_BLOOD2  4
#define PT_BLASTER 5
#define PT_SMOKE   6
#define PT_SPLASH  7
#define PT_SPARK   8
#define PT_BEAM    9
#define PT_SPIRAL  10
#define PT_FLAME   11
#define PT_BLOODSPRAY	12
#define PT_xBLOODSPRAY	13
#define	PT_EXPLODE		14
#define	PT_WATERPULME	15
#define	PT_WATERCIRCLE	16
#define PT_BLOODDRIP	17
#define PT_BLOODMIST	18
#define PT_BLOOD_SPLAT  19
#define PT_BLASTER_BOLT 20
#define PT_MAX     21


#define DECAL_BULLET		0
#define DECAL_BLASTER		1
#define DECAL_EXPLODE		2
#define DECAL_RAIL			3
#define DECAL_BLOOD1			4
#define DECAL_BLOOD2			5
#define DECAL_BLOOD3			6
#define DECAL_BLOOD4			7
#define DECAL_BLOOD5			8
#define DECAL_BLOOD6			9
#define DECAL_BLOOD7			10
#define DECAL_BLOOD8			11
#define DECAL_BLOOD9			12
#define DECAL_ACIDMARK			13
#define DECAL_BFG				14


#define DECAL_MAX			15

#define DF_SHADE		0x00000400	// 1024
#define DF_NOTIMESCALE	0x00000800	// 2048
#define INSTANT_DECAL	-10000.0
#define DF_OVERBRIGHT	1
#define DF_VERTEXLIGHT	2
#define DF_AREADECAL	4

#endif
